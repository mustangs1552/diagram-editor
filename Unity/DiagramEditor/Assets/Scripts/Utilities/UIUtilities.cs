﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace Assets.Scripts.Utilities
{
    public static class UIUtilities
    {
        public static List<T> CastRayFromMouse<T>()
        {
            PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
            pointerEventData.position = Input.mousePosition;
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerEventData, results);

            List<T> returnResults = new List<T>();
            foreach(RaycastResult obj in results)
            {
                T resultType = obj.gameObject.GetComponent<T>();
                if (resultType != null) returnResults.Add(resultType);
            }
            return returnResults;
        }
    }

    public enum Directions
    {
        None,

        Up,
        Down,
        Left,
        Right,

        All,
        Horizontal,
        Vertical,

        UpLeft,
        UpRight,
        UpHorizontal,

        DownLeft,
        DownRight,
        DownHorizontal
    }
}
