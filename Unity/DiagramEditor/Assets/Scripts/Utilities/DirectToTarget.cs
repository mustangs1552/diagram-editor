﻿using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public class DirectToTarget : MonoBehaviour
    {
        [SerializeField] private GameObject target = null;
        public GameObject Target
        {
            get
            {
                return target;
            }
        }
    }
}
