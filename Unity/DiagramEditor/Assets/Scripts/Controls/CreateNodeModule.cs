﻿using UnityEngine;

namespace Assets.Scripts.Controls
{
    public class CreateNodeModule : EditMenuModule
    {
        [SerializeField] private GameObject nodePrefab = null;

        public override void OnClick()
        {
            GameObject newNode = Instantiate(nodePrefab, new Vector3(MainMenu.MenuTargetPos.x, MainMenu.MenuTargetPos.y, 0), Quaternion.identity);
            DiagramController.SINGLETON?.ActiveDiagram?.AddUIObject(newNode.GetComponent<UIObject>());

            MainMenu.Close();
        }

        private void Setup()
        {
            if (nodePrefab == null) Debug.LogError("No 'nodePrefab' set!");
        }

        private void Awake()
        {
            Setup();
        }
    }
}
