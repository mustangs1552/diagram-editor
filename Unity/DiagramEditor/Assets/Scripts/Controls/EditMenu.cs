﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Controls
{
    /// <summary>
    /// The main script for the edit menu.
    /// Manages visibility and the buttons that belong to it.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class EditMenu : MonoBehaviour
    {
        #region Variables
        [Tooltip("The rect transform of the menu object.")]
        [SerializeField] private RectTransform menu = null;
        [Tooltip("The prefab for the buttons.")]
        [SerializeField] private GameObject moduleButtonPrefab = null;
        [Tooltip("The height of each button.")]
        [SerializeField] private float moduleButtonHeight = 30;
        [Tooltip("The anchor object where the buttons are spawned.")]
        [SerializeField] private Transform modulesAnchor = null;

        /// <summary>
        /// Is this menu open?
        /// </summary>
        public bool IsOpen
        {
            get
            {
                return gameObject.activeInHierarchy;
            }
        }
        /// <summary>
        /// The current object that is being targeted by the edit menu.
        /// </summary>
        public UIObject Target
        {
            get
            {
                return currTarget;
            }
        }
        public Vector2 MenuTargetPos
        {
            get
            {
                return menuTargetPos;
            }
        }

        private RectTransform rectTrans = null;
        private float startingZ = 0;
        private UIObject currTarget = null;
        private EditMenuModule[] modules = null;
        private List<Button> buttons = new List<Button>();
        private Vector2 menuTargetPos = Vector2.zero;
        #endregion

        #region Methods
        /// <summary>
        /// Open and setup this edit menu.
        /// </summary>
        /// <param name="target">The target object to be edited.</param>
        /// <param name="position">The desired position of the menu.</param>
        public void Open(UIObject target, Vector2 position)
        {
            if (modules != null && modules.Length > 0)
            {
                menuTargetPos = position + new Vector2(menu.rect.width * rectTrans.localScale.x / 2, menu.rect.height * rectTrans.localScale.y / -2);
                transform.position = new Vector3(menuTargetPos.x, menuTargetPos.y, startingZ);
                currTarget = target;
                RemoveButtons();
                gameObject.SetActive(true);
                AddButtons();
            }
        }
        /// <summary>
        /// Close this edit menu.
        /// </summary>
        public void Close()
        {
            RemoveButtons();
            gameObject.SetActive(false);
            currTarget = null;
            menuTargetPos = Vector2.zero;
        }

        public void RunSetup()
        {
            Setup();
        }

        #region Private
        /// <summary>
        /// Add all the needed buttons and update the size of the edit menu to fit.
        /// </summary>
        private void AddButtons()
        {
            foreach(EditMenuModule module in modules)
            {
                if (module.VisibilityRequirement == ButtonVisibilityRequirement.Target && Target != null) AddButton(module);
                else if (module.VisibilityRequirement == ButtonVisibilityRequirement.NoTarget && Target == null) AddButton(module);
                else if (module.VisibilityRequirement == ButtonVisibilityRequirement.Either) AddButton(module);
            }

            rectTrans.sizeDelta = new Vector2(rectTrans.rect.width, moduleButtonHeight * buttons.Count);
        }
        /// <summary>
        /// Remove all the buttons.
        /// </summary>
        private void RemoveButtons()
        {
            foreach (Button button in buttons) Destroy(button.gameObject);
            buttons = new List<Button>();
        }

        /// <summary>
        /// Add the button for the given module.
        /// </summary>
        /// <param name="module">The module to be added as a button.</param>
        private void AddButton(EditMenuModule module)
        {
            GameObject modButtonGO = Instantiate(moduleButtonPrefab, transform.position, Quaternion.identity, modulesAnchor);
            Button modButton = modButtonGO.GetComponent<Button>();
            if (modButton != null)
            {
                modButton.transform.Find("Text").GetComponent<Text>().text = module.ModuleName;
                modButton.onClick.AddListener(module.OnClick);
                buttons.Add(modButton);
            }
            else
            {
                Debug.LogError("Spawned button doesn't have a 'Button' component!");
                Destroy(modButtonGO);
            }
        }

        /// <summary>
        /// Find all the modules on this object.
        /// </summary>
        private void FindModules()
        {
            modules = GetComponents<EditMenuModule>();

            if (modules == null || modules.Length == 0) Debug.LogError("No 'EditMenuModules' found!");
            else
            {
                foreach (EditMenuModule module in modules) module.MainMenu = this;
                rectTrans.sizeDelta = new Vector2(rectTrans.rect.width, moduleButtonHeight * modules.Length);
            }
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            startingZ = transform.localPosition.z;

            rectTrans = GetComponent<RectTransform>();
            if (menu == null) Debug.LogError("No 'menu' assigned!");
            if (moduleButtonPrefab == null) Debug.LogError("No 'moduleButtonPrefab' assigned!");
            if (modulesAnchor == null) Debug.LogError("No 'modulesAnchor' assigned!");

            FindModules();
        }
        #endregion
        #endregion

        private void Awake()
        {
            Setup();
        }
    }

    /// <summary>
    /// The requirement for a button to be shown in the edit menu.
    /// </summary>
    public enum ButtonVisibilityRequirement
    {
        NoTarget,
        Target,
        Either
    }
}
