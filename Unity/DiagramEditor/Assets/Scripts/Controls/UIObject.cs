﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Controls
{
    /// <summary>
    /// A UI object that each interactable node will inherit from.
    /// It has basic interaction controls that all UI objects will have.
    /// </summary>
    public class UIObject : MonoBehaviour
    {
        #region Variables
        [Tooltip("The color of the background when this object is selected.")]
        [SerializeField] private Color selectedColor = Color.yellow;
        [Tooltip("This object's background image.")]
        [SerializeField] private Image backgroundImage = null;
        [Tooltip("The time between double clicks in order for them to be considered a double click.")]
        [SerializeField] private float doubleClickTime = .25f;

        /// <summary>
        /// Called when this object was single clicked.
        /// </summary>
        public Action OnSingleClick { get; set; }
        /// <summary>
        /// Called when this object was double clicked.
        /// </summary>
        public Action OnDoubleClick { get; set; }
        public Action OnDesselect { get; set; }
        /// <summary>
        /// Is the delay after right clicking done?
        /// </summary>
        public bool IsRightClickDelayActive
        {
            get
            {
                return Time.time - lastRightClick < rightClickDelay + doubleClickTime;
            }
        }

        private Color defaultColor = Color.white;
        private float firstClick = 0;
        private Vector2 lastMousePos = Vector2.zero;
        Vector2 currMousePos = Vector2.zero;
        private bool wasDragging = false;
        private float rightClickDelay = .1f;
        private float lastRightClick = 0;
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// The mouse clicked on this object.
        /// Determine if it should be a single or double click.
        /// </summary>
        public void OnClick()
        {
            if (firstClick == 0)
            {
                firstClick = Time.time;
                Invoke("CheckIfDoubleClickedAfterOnClick", doubleClickTime);
            }
            else
            {
                firstClick = 0;
                DoubleClickObject();
            }
        }
        /// <summary>
        /// This object is being dragged.
        /// </summary>
        public void OnDrag()
        {
            currMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (lastMousePos != Vector2.zero) transform.Translate(currMousePos - lastMousePos);
            lastMousePos = currMousePos;
            wasDragging = true;
        }

        /// <summary>
        /// Select this object.
        /// </summary>
        public void SelectObject()
        {
            backgroundImage.color = selectedColor;
            UIMainControls.SINGLETON.UIObjectSelected(this);
        }
        /// <summary>
        /// Deselect this object.
        /// </summary>
        public void Deselect()
        {
            backgroundImage.color = defaultColor;
            OnDesselect?.Invoke();
        }
        #endregion

        #region Private
        /// <summary>
        /// Single click this object.
        /// </summary>
        private void SingleClickObject()
        {
            if (wasDragging) EndDragging();
            else if(!IsRightClickDelayActive)
            {
                SelectObject();
                OnSingleClick?.Invoke();
            }
        }
        /// <summary>
        /// Double click this object.
        /// </summary>
        private void DoubleClickObject()
        {
            OnDoubleClick?.Invoke();
        }

        /// <summary>
        /// End dragging and reset saved mouse position.
        /// </summary>
        private void EndDragging()
        {
            wasDragging = false;
            lastMousePos = Vector2.zero;
        }

        /// <summary>
        /// Check to see if this object was double clicked, if no then do the single click.
        /// </summary>
        private void CheckIfDoubleClickedAfterOnClick()
        {
            if (firstClick != 0)
            {
                SingleClickObject();
                firstClick = 0;
            }
        }

        /// <summary>
        /// Check for user input.
        /// </summary>
        private void CheckInput()
        {
            if (Input.GetMouseButtonUp(1)) lastRightClick = Time.time;
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (backgroundImage == null) Debug.LogError("No 'backgroundImage' assigned!");
            else defaultColor = backgroundImage.color;
        }
        #endregion

        #region Unity Methods
        private void Awake()
        {
            Setup();
        }
        private void Update()
        {
            CheckInput();
        }
        #endregion
        #endregion
    }
}
