﻿using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Utilities;

namespace Assets.Scripts.Controls
{
    /// <summary>
    /// The main class for managing general user input and the various UIObjects.
    /// </summary>
    public class UIMainControls : MonoBehaviour
    {
        #region Variables
        public static UIMainControls SINGLETON = null;

        [Tooltip("Distance to zoom each step.")]
        [SerializeField] private float zoomScale = 1;
        [Tooltip("Farthest you can zoom out.")]
        [SerializeField] private float maxZoomOut = 10;
        [Tooltip("Panning speed of the camera. The farther away from zero the slower.")]
        [SerializeField] private float panSpeed = 50;
        [Tooltip("The edit menu to use.")]
        [SerializeField] private EditMenu editMenu = null;

        private UIObject selectedObj = null;
        private Vector2 lastMousePos = Vector2.zero;
        private Vector2 currMousePos = Vector2.zero;
        private bool wasDragging = false;
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// A click on an empty space.
        /// </summary>
        public void GlobalClick()
        {
            if (wasDragging) EndDragging();
            else DeselectSelectedObj();
        }

        /// <summary>
        /// Pan the camera to follow mouse drag.
        /// </summary>
        public void OnDrag()
        {
            currMousePos = Input.mousePosition;
            if(lastMousePos != Vector2.zero) Camera.main.transform.Translate((currMousePos - lastMousePos) / panSpeed);
            lastMousePos = currMousePos;
            wasDragging = true;
        }

        /// <summary>
        /// A UIObject was selected.
        /// </summary>
        /// <param name="uiObj">The UIObject that was selected.</param>
        public void UIObjectSelected(UIObject uiObj)
        {
            if (selectedObj != null && uiObj == selectedObj) return;

            DeselectSelectedObj();
            selectedObj = uiObj;
        }
        #endregion

        #region Private
        /// <summary>
        /// Deselect the UIObject that is currently selected.
        /// </summary>
        private void DeselectSelectedObj()
        {
            if (selectedObj != null)
            {
                selectedObj.Deselect();
                selectedObj = null;
            }
        }

        /// <summary>
        /// Zoom the camera in or out.
        /// </summary>
        private void PerformZoom()
        {
            float newSize = Camera.main.orthographicSize - Input.mouseScrollDelta.y * zoomScale;
            if (newSize < 1) newSize = 1;
            else if (newSize > maxZoomOut) newSize = maxZoomOut;
            Camera.main.orthographicSize = newSize;
        }

        /// <summary>
        /// Zoom the camera on mouse scroll.
        /// </summary>
        private void OnMouseScroll()
        {
            PerformZoom();
        }

        /// <summary>
        /// Open the edit menu.
        /// </summary>
        /// <param name="uiObj">The UIObject to be targeted.</param>
        /// <param name="position">The desired position of the edit menu.</param>
        private void OpenEditMenu(UIObject uiObj, Vector2 position)
        {
            if (editMenu != null) editMenu.Open(uiObj, position);
        }

        /// <summary>
        /// The user left clicked.
        /// </summary>
        private void OnLeftClick()
        {
            List<DirectToTarget> results = UIUtilities.CastRayFromMouse<DirectToTarget>();
            if (results.Count == 0 || (results.Count > 0 && results[0].Target != editMenu.gameObject)) editMenu.Close();
        }
        /// <summary>
        /// The user right clicked.
        /// </summary>
        private void OnRightClick()
        {
            List<DirectToTarget> results = UIUtilities.CastRayFromMouse<DirectToTarget>();
            if (results.Count > 0)
            {
                UIObject uiObj = results[0].Target.GetComponent<UIObject>();
                if (uiObj != null) OpenEditMenu(uiObj, Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
            else OpenEditMenu(null, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        /// <summary>
        /// End dragging and reset saved mouse position.
        /// </summary>
        private void EndDragging()
        {
            wasDragging = false;
            lastMousePos = Vector2.zero;
        }

        /// <summary>
        /// Check on input from the user.
        /// </summary>
        private void CheckInput()
        {
            if (Input.mouseScrollDelta.y != 0) OnMouseScroll();

            if (Input.GetMouseButtonDown(0)) OnLeftClick();
            if (Input.GetMouseButtonUp(1)) OnRightClick();
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (UIMainControls.SINGLETON == null) SINGLETON = this;
            else
            {
                Debug.LogError("More than one '" + SINGLETON.GetType() + "' found!");
                Destroy(this);
            }

            if (editMenu != null)
            {
                editMenu.RunSetup();
                editMenu.Close();
            }
        }
        #endregion

        #region Unity Methods
        private void Awake()
        {
            Setup();
        }
        private void Update()
        {
            CheckInput();
        }
        #endregion
        #endregion
    }
}
