﻿using UnityEngine;

namespace Assets.Scripts.Controls
{
    public class DiagramController : MonoBehaviour
    {
        public static DiagramController SINGLETON = null;

        public Diagram ActiveDiagram { get; set; }

        private void Awake()
        {
            if (DiagramController.SINGLETON == null) SINGLETON = this;
            else
            {
                Debug.LogError("More than one 'DiagramController' singletons detected!");
                Destroy(this);
            }
        }
    }
}
