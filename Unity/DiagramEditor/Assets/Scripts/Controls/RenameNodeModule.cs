﻿using UnityEngine;

namespace Assets.Scripts.Controls
{
    public class RenameNodeModule : EditMenuModule
    {
        public override void OnClick()
        {
            IRenamable renamable = MainMenu.Target.GetComponent<IRenamable>();
            if (renamable != null) renamable.StartRename();

            MainMenu.Target.SelectObject();
            MainMenu.Close();
        }
    }
}
