﻿namespace Assets.Scripts.Controls
{
    public interface IRenamable
    {
        void StartRename();
    }
}
