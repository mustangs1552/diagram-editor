﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Controls
{
    [RequireComponent(typeof(UIObject))]
    [RequireComponent(typeof(Diagram))]
    public class StandardNode : MonoBehaviour, IRenamable
    {
        [SerializeField] private Text nameText = null;
        [SerializeField] private InputField inputField = null;

        private UIObject uiObj = null;
        private Diagram diagram = null;

        public void StartRename()
        {
            EnableRenaming();
        }
        public void StopRenaming(bool isCanceling)
        {
            DisableRenaming(isCanceling);
        }

        public void EnterNode()
        {
            diagram.OpenDiagram();
        }

        private void EnableRenaming()
        {
            nameText.gameObject.SetActive(false);
            inputField.gameObject.SetActive(true);
            uiObj.OnDesselect += Deselected;

            inputField.Select();
        }
        private void DisableRenaming(bool isCanceling)
        {
            if(!isCanceling) nameText.text = inputField.text;

            nameText.gameObject.SetActive(true);
            inputField.DeactivateInputField();
            inputField.gameObject.SetActive(false);

            uiObj.OnDesselect -= Deselected;
        }

        private void Deselected()
        {
            DisableRenaming(false);
        }

        private void CheckInput()
        {
            if (inputField.isFocused && Input.GetKeyUp(KeyCode.Return)) DisableRenaming(false);
        }

        private void Setup()
        {
            uiObj = GetComponent<UIObject>();
            diagram = GetComponent<Diagram>();

            if (nameText == null) Debug.LogError("No 'nameText' assigned!");
            if (inputField == null) Debug.LogError("No 'inputField' assigned!");

            DisableRenaming(true);
        }

        private void Awake()
        {
            Setup();
        }
        private void Update()
        {
            CheckInput();
            if (inputField.wasCanceled) DisableRenaming(true);
        }
    }
}
