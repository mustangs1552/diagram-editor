﻿using UnityEngine;

namespace Assets.Scripts.Controls
{
    /// <summary>
    /// This is the base class for a button in the edit menu.
    /// </summary>
    public abstract class EditMenuModule : MonoBehaviour
    {
        [Tooltip("The name to show on the module's button.")]
        [SerializeField] private string moduleName = "";
        [Tooltip("When shhould this button be visible in the edit menu?")]
        [SerializeField] private ButtonVisibilityRequirement visibilityRequirment = ButtonVisibilityRequirement.Either;

        /// <summary>
        /// The name to show on the module's button.
        /// </summary>
        public string ModuleName
        {
            get
            {
                return moduleName;
            }
        }
        /// <summary>
        /// When shhould this button be visible in the edit menu?
        /// </summary>
        public ButtonVisibilityRequirement VisibilityRequirement
        {
            get
            {
                return visibilityRequirment;
            }
        }
        /// <summary>
        /// The edit menu that this module belongs to.
        /// </summary>
        public EditMenu MainMenu { get; set; }

        /// <summary>
        /// This button was clicked on.
        /// </summary>
        public abstract void OnClick();
    }
}
