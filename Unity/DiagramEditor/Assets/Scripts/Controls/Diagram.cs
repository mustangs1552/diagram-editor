﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Controls
{
    public class Diagram : MonoBehaviour
    {
        private List<UIObject> uiObjects = new List<UIObject>();

        public void OpenDiagram()
        {
            if (DiagramController.SINGLETON != null && DiagramController.SINGLETON.ActiveDiagram != null) DiagramController.SINGLETON.ActiveDiagram.CloseDiagram();

            ShowNodes();
            ShowConnections();
        }
        public void CloseDiagram()
        {

        }

        public void AddUIObject(UIObject uiObj)
        {
            if (uiObj != null) uiObjects.Add(uiObj);
        }
        public void RemoveUIObject(UIObject uiObj)
        {
            if (uiObj != null) Destroy(uiObj.gameObject);
        }

        private void ShowNodes()
        {

        }
        private void ShowConnections()
        {

        }
    }
}
