﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using TMPro;

namespace Assets.Scripts
{
    public class UIController : MonoBehaviour
    {
        public static UIController SINGLETON = null;

        [Tooltip("The TMP text header showing name of current diagram.")]
        public TextMeshProUGUI diagramHeader = null;
        [Tooltip("The exit node button.")]
        public Button exitNodeButton = null;
        [Tooltip("The breadcrumb trail of entered diagrams dropdown.")]
        public TMP_Dropdown diagramBCTDropdown = null;
        public UnityEvent OnEnteredTextbox = new UnityEvent();
        public UnityEvent OnExitedTextbox = new UnityEvent();

        /// <summary>
        /// Is the user currently in a text box?
        /// </summary>
        public bool IsInTextBox
        {
            get => isInTextBox;
            set
            {
                if(value != isInTextBox)
                {
                    if (value) OnEnteredTextbox?.Invoke();
                    else OnExitedTextbox?.Invoke();

                    isInTextBox = value;
                }
            }
        }

        protected bool autoSelected = false;
        protected bool isInTextBox = false;

        /// <summary>
        /// Update the text of the diagram name header.
        /// </summary>
        public virtual void UpdateDiagramHeader()
        {
            if (!DiagramController.SINGLETON || !diagramHeader) return;

            List<string> diagramNames = DiagramController.SINGLETON.DiagramNames;
            if (diagramNames.Count == 0) return;
            diagramHeader.text = (diagramNames.Count == 1) ? diagramNames[0] : diagramNames[1];
        }

        /// <summary>
        /// Hide the exit node button when in the root diagram.
        /// </summary>
        public virtual void UpdateExitNodeButtonVisibility()
        {
            if (!DiagramController.SINGLETON || !exitNodeButton) return;

            exitNodeButton.gameObject.SetActive(!DiagramController.SINGLETON.IsRootDiagram);
        }

        /// <summary>
        /// Update the list of options in the dropdown.
        /// </summary>
        public virtual void UpdateDiagramBreadcrumbTrail()
        {
            if (!DiagramController.SINGLETON || !diagramBCTDropdown) return;

            List<TMP_Dropdown.OptionData> options = new List<TMP_Dropdown.OptionData>();
            foreach (string diagramName in DiagramController.SINGLETON.DiagramNames) options.Add(new TMP_Dropdown.OptionData(diagramName));
            diagramBCTDropdown.options = options;
            autoSelected = true;
            diagramBCTDropdown.value = options.Count - 1;
            autoSelected = false;
        }
        /// <summary>
        /// Enter the diagram that was selected at the given dropdown index.
        /// </summary>
        /// <param name="index">The index of the selected dropdown option.</param>
        public virtual void SelectDiagram(int index)
        {
            if (autoSelected || !DiagramController.SINGLETON) return;

            DiagramController.SINGLETON.EnterDiagramInPath(index);
        }

        /// <summary>
        /// Quit the application.
        /// </summary>
        public virtual void Quit()
        {
            Application.Quit();
        }
        /// <summary>
        /// Reload the current scene.
        /// </summary>
        public virtual void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        protected virtual void Awake()
        {
            if (!UIController.SINGLETON) SINGLETON = this;
            else
            {
                Debug.LogError($"More than one '{GetType()}' singletons found, destroying second...");
                Destroy(this);
            }
        }
    }
}
