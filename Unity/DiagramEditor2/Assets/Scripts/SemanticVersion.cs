﻿// Follows Semantic Versioning: https://semver.org/

namespace Assets.Scripts
{
    public class SemanticVersion
    {
        public readonly int major = 0;
        public readonly int minor = 0;
        public readonly int patch = 0;
        public readonly string preRelease = "";
        public readonly string build = "";

        public SemanticVersion()
        {

        }
        /// <summary>
        /// Parses the given string semantic version into this object.
        /// Must be in the semantic version format "uint.uint.uint-preRelease+build" (pre-release and build optional).
        /// </summary>
        /// <param name="version">The semantic version string.</param>
        public SemanticVersion(string version)
        {
            SemanticVersion parsedVersion = FromString(version);
            if (major >= 0) this.major = parsedVersion.major;
            if (minor >= 0) this.minor = parsedVersion.minor;
            if (patch >= 0) this.patch = parsedVersion.patch;
            if (!string.IsNullOrEmpty(preRelease)) this.preRelease = parsedVersion.preRelease;
            if (!string.IsNullOrEmpty(build)) this.build = parsedVersion.build;
        }
        /// <summary>
        /// Applies the given values to this object.
        /// </summary>
        /// <param name="major">The major version of semantic versioning. Must be a `uint` value.</param>
        /// <param name="minor">The minor version of semantic versioning. Must be a `uint` value.</param>
        /// <param name="patch">The patch version of semantic versioning. Must be a `uint` value.</param>
        /// <param name="preRelease">Pre-release text of semantic version.</param>
        /// <param name="build">Build text of semantic version.</param>
        public SemanticVersion(int major, int minor, int patch, string preRelease = "", string build = "")
        {
            if (major >= 0) this.major = major;
            if (minor >= 0) this.minor = minor;
            if (patch >= 0) this.patch = patch;
            if (!string.IsNullOrEmpty(preRelease)) this.preRelease = preRelease;
            if (!string.IsNullOrEmpty(build)) this.build = build;
        }

        /// <summary>
        /// Parses given string semantic version to a `SemanticVersion` object.
        /// Must be in the semantic version format "uint.uint.uint-preRelease+build" (pre-release and build optional).
        /// </summary>
        /// <param name="version">The semantic version string.</param>
        /// <returns>The parsed `SemanticVersion`.</returns>
        public virtual SemanticVersion FromString(string version)
        {
            string[] parts = version.Split('.', '-', '+');
            if (parts.Length < 3 || parts.Length >= 5) return new SemanticVersion();

            int major = 0;
            int.TryParse(parts[0], out major);
            int minor = 0;
            int.TryParse(parts[1], out minor);
            int patch = 0;
            int.TryParse(parts[2], out patch);

            string preRelease = "";
            if (parts.Length > 3 && !string.IsNullOrEmpty(parts[3])) preRelease = parts[3];
            string build = "";
            if (parts.Length > 4 && !string.IsNullOrEmpty(parts[4])) build = parts[4];

            return new SemanticVersion(major, minor, patch, preRelease, build);
        }
        /// <summary>
        /// Parses this object to a string in the format "major.minor.patch-preRelease+build".
        /// </summary>
        /// <returns>This object as a string.</returns>
        public override string ToString()
        {
            string version = $"{major}.{minor}.{patch}";
            if (!string.IsNullOrEmpty(preRelease)) version += $"-{preRelease}";
            if (!string.IsNullOrEmpty(build)) version += $"+{build}";

            return version;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static bool operator ==(SemanticVersion a, SemanticVersion b)
        {
            return a.Equals(null) && b.Equals(null) ||
                  (!a.Equals(null) && !b.Equals(null) && a.major == b.major && a.minor == b.minor && a.patch == b.patch && a.preRelease == b.preRelease && a.build == b.build);
        }
        public static bool operator !=(SemanticVersion a, SemanticVersion b)
        {
            return (a.Equals(null) && !b.Equals(null)) ||
                   (!a.Equals(null) && b.Equals(null)) ||
                   (!a.Equals(null) && !b.Equals(null) && (a.major != b.major || a.minor != b.minor || a.patch != b.patch || a.preRelease != b.preRelease || a.build != b.build));
        }
        public static bool operator >(SemanticVersion a, SemanticVersion b)
        {
            return a != b &&
                  !a.Equals(null) && !b.Equals(null) &&
                   (a.major > b.major ||
                   (a.major == b.major && a.minor > b.minor) ||
                   (a.major == b.major && a.minor == b.minor && a.patch > b.patch));
        }
        public static bool operator <(SemanticVersion a, SemanticVersion b)
        {
            return a != b &&
                  !a.Equals(null) && !b.Equals(null) &&
                   (a.major < b.major ||
                   (a.major == b.major && a.minor < b.minor) ||
                   (a.major == b.major && a.minor == b.minor && a.patch < b.patch));
        }
        public static bool operator >=(SemanticVersion a, SemanticVersion b)
        {
            return a == b || a > b;
        }
        public static bool operator <=(SemanticVersion a, SemanticVersion b)
        {
            return a == b || a < b;
        }
    }
}
