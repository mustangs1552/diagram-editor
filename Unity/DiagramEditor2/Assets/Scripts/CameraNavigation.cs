﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;

namespace Assets.Scripts
{
    public class CameraNavigation : MonoBehaviour
    {
        [Tooltip("The camera to be dragged. Will use object's existing 'Camera' component if null.")]
        [SerializeField] protected Camera targetCamera = null;
        [Header("Dragging")]
        [Tooltip("The speed of the drag.")]
        public Vector2 dragSpeed = Vector2.one;
        [Tooltip("The dragable area.")]
        [SerializeField] protected Vector2 dragArea = Vector2.zero;
        [Tooltip("Draw the gizmo that shows the drag area?")]
        public bool drawDragAreaGizmo = false;
        [Tooltip("Delay before starting drag after initial press.")]
        [SerializeField] protected float delay = 0;
        [Header("Zooming")]
        [Tooltip("The speed of the zoom.")]
        public float zoomSpeed = 10;
        [Tooltip("Minimum zoom level.")]
        [SerializeField] protected float minZoom = 1;
        [Tooltip("Maximum zoom level.")]
        [SerializeField] protected float maxZoom = 10;

        /// <summary>
        /// Delay before starting drag after initial press.
        /// </summary>
        public float Delay
        {
            get => delay;
            set
            {
                if (value >= 0) delay = value;
            }
        }

        /// <summary>
        /// The percent difference between the current zoom level and the start zoom level.
        /// </summary>
        public float CurrCameraZoomPerc
        {
            get
            {
                if (!targetCamera) return 0;

                if (!targetCamera.orthographic) return targetCamera.fieldOfView / startCameraZoom;
                else return targetCamera.orthographicSize / startCameraZoom;
            }
        }

        /// <summary>
        /// The dragable area.
        /// </summary>
        public Vector2 DragArea
        {
            get => new Vector2(dragArea.x, dragArea.y);
            set
            {
                if (dragArea == null) return;

                dragArea = new Vector2((value.x < 0) ? 0 : value.x, (value.y < 0) ? 0 : value.y);
            }
        }

        /// <summary>
        /// Minimum zoom level.
        /// </summary>
        public float MinZoom
        {
            get => minZoom;
            set
            {
                if (value < 0 || value > MaxZoom) return;

                minZoom = value;
            }
        }
        /// <summary>
        /// Maximum zoom level.
        /// </summary>
        public float MaxZoom
        {
            get => maxZoom;
            set
            {
                if (value < 0 || value < MinZoom) return;

                maxZoom = value;
            }
        }

        protected Vector3 startCameraPos = Vector3.zero;
        protected Vector3 lastMousePos = Vector3.zero;
        protected Vector3 lastCameraPos = Vector3.zero;
        protected bool wasDragging = false;
        protected Vector3 currPosDiff = Vector3.zero;
        protected float startTime = -1;
        protected float startCameraZoom = 0;
        protected Vector2 currDragAreaLimitWithCameraEdge = Vector2.zero;
        protected Vector2 currDragAreaLimitWithCameraEdgeMultiplier = Vector2.zero;

        /// <summary>
        /// Prep before dragging.
        /// </summary>
        public virtual void PreDrag()
        {
            startTime = Time.time;
        }
        /// <summary>
        /// Drag the camera by the mouse's starting position and after delay has passed.
        /// </summary>
        public virtual void DragCamera()
        {
            if (!targetCamera) return;

            if(!wasDragging && Time.time - startTime >= Delay)
            {
                wasDragging = true;
                lastMousePos = targetCamera.ScreenToWorldPoint(Input.mousePosition);
                lastCameraPos = targetCamera.transform.position;
            }
            else if(wasDragging)
            {
                currPosDiff = targetCamera.ScreenToWorldPoint(Input.mousePosition) - lastMousePos;
                targetCamera.transform.position += new Vector3(currPosDiff.x * -dragSpeed.x, currPosDiff.y * -dragSpeed.y, 0);

                lastMousePos = targetCamera.ScreenToWorldPoint(Input.mousePosition);
                lastCameraPos = targetCamera.transform.position;

                CheckDragAreaLimits();
            }
        }
        /// <summary>
        /// Clean up after draggins.
        /// </summary>
        public virtual void FinishDrag()
        {
            wasDragging = false;
            lastMousePos = Vector3.zero;
            startTime = -1;
        }

        /// <summary>
        /// Zoom the camera view in or out.
        /// </summary>
        /// <param name="input">Amount and direction to zoom.</param>
        public virtual void Zoom(float input)
        {
            if (!targetCamera) return;

            if (!targetCamera.orthographic)
            {
                targetCamera.fieldOfView += input * -zoomSpeed;

                if (targetCamera.fieldOfView > maxZoom) targetCamera.fieldOfView = maxZoom;
                else if (targetCamera.fieldOfView < minZoom) targetCamera.fieldOfView = minZoom;
            }
            else
            {
                targetCamera.orthographicSize += input * -zoomSpeed;

                if (targetCamera.orthographicSize > maxZoom) targetCamera.orthographicSize = maxZoom;
                else if (targetCamera.orthographicSize < minZoom) targetCamera.orthographicSize = minZoom;
            }

            CheckDragAreaLimits();
        }

        /// <summary>
        /// Check if still within allowed draggable area and correct if not.
        /// </summary>
        protected virtual void CheckDragAreaLimits()
        {
            if (!targetCamera) return;

            currDragAreaLimitWithCameraEdgeMultiplier = (Screen.currentResolution.width > Screen.currentResolution.height) ? new Vector2((float)Screen.currentResolution.width / (float)Screen.currentResolution.height, 1) : new Vector2(1, (float)Screen.currentResolution.height / (float)Screen.currentResolution.width);
            if(!targetCamera.orthographic) currDragAreaLimitWithCameraEdge = new Vector2(targetCamera.fieldOfView * currDragAreaLimitWithCameraEdgeMultiplier.x, targetCamera.fieldOfView * currDragAreaLimitWithCameraEdgeMultiplier.y);
            else currDragAreaLimitWithCameraEdge = new Vector2(targetCamera.orthographicSize * currDragAreaLimitWithCameraEdgeMultiplier.x, targetCamera.orthographicSize * currDragAreaLimitWithCameraEdgeMultiplier.y);

            if (targetCamera.transform.position.x - startCameraPos.x > DragArea.x / 2 - currDragAreaLimitWithCameraEdge.x) targetCamera.transform.position = new Vector3(startCameraPos.x + DragArea.x / 2 - currDragAreaLimitWithCameraEdge.x, targetCamera.transform.position.y, targetCamera.transform.position.z);
            else if (startCameraPos.x - targetCamera.transform.position.x > DragArea.x / 2 - currDragAreaLimitWithCameraEdge.x) targetCamera.transform.position = new Vector3(startCameraPos.x - DragArea.x / 2 + currDragAreaLimitWithCameraEdge.x, targetCamera.transform.position.y, targetCamera.transform.position.z);

            if (targetCamera.transform.position.y - startCameraPos.y > DragArea.y / 2 - currDragAreaLimitWithCameraEdge.y) targetCamera.transform.position = new Vector3(targetCamera.transform.position.x, startCameraPos.y + DragArea.y / 2 - currDragAreaLimitWithCameraEdge.y, targetCamera.transform.position.z);
            else if (startCameraPos.y - targetCamera.transform.position.y > DragArea.y / 2 - currDragAreaLimitWithCameraEdge.y) targetCamera.transform.position = new Vector3(targetCamera.transform.position.x, startCameraPos.y - DragArea.y / 2 + currDragAreaLimitWithCameraEdge.y, targetCamera.transform.position.z);
        }

        /// <summary>
        /// Set starting values.
        /// </summary>
        protected virtual void SetStartValues()
        {
            if(targetCamera)
            {
                startCameraPos = targetCamera.transform.position;

                if (!targetCamera.orthographic) startCameraZoom = targetCamera.fieldOfView;
                else startCameraZoom = targetCamera.orthographicSize;
            }
        }

        /// <summary>
        /// Check for missing required values.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (!targetCamera) UnityLoggingUtility.LogMissingValue(GetType(), "targetCamera", gameObject);
        }

        protected virtual void OnValidate()
        {
            if (delay < 0) delay = 0;
            DragArea = dragArea;
            MinZoom = minZoom;
            MaxZoom = maxZoom;
        }

        protected virtual void Awake()
        {
            if (!targetCamera) targetCamera = GetComponent<Camera>();

            CheckMissingValues();
            SetStartValues();
        }

        protected virtual void OnDrawGizmos()
        {
            if (drawDragAreaGizmo) Gizmos.DrawCube(Vector3.zero, new Vector3(dragArea.x, dragArea.y, 0));
        }
    }
}
