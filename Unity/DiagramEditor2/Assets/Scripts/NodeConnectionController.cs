﻿using UnityEngine;
using MattRGeorge.Unity.Utilities;

namespace Assets.Scripts
{
    public enum ConnectionMode
    {
        None,
        Connecting,
        Deleting
    }

    public class NodeConnectionController : MonoBehaviour
    {
        public static NodeConnectionController SINGLETON = null;

        [Tooltip("The line renderer to use to show a pending connection selection.")]
        [SerializeField] protected LineRenderer pendingConnectionLine = null;
        [Tooltip("The line renderer to use to show a pending connection deletion.")]
        [SerializeField] protected LineRenderer pendingDeletionLine = null;
        [Tooltip("What Unity update method should update the pending lines' positions?")]
        public UnityUpdateMethods updateMethod = UnityUpdateMethods.Update;
        [Tooltip("Use mouse's position in the world or screen space?")]
        public bool useMouseWorldPosition = false;

        /// <summary>
        /// The current active connection mode.
        /// </summary>
        public ConnectionMode CurrConnectionMode { get; set; } = ConnectionMode.None;
        /// <summary>
        /// The current starting node for a pending connection selection or deletion.
        /// </summary>
        public ConnectableNode StartNode { get; set; } = null;
        /// <summary>
        /// Is the starting node handle that started the current connection mode an out or in handle?
        /// </summary>
        public bool IsStartNodeOutHandle { get; set; } = false;

        protected Vector3 pendingLineStartPos = Vector3.zero;
        protected Vector3 pendingLineEndPos = Vector3.zero;

        /// <summary>
        /// Cancel the current pending connection.
        /// </summary>
        public virtual void CancelConnectionSelection()
        {
            CurrConnectionMode = ConnectionMode.None;
            StartNode = null;
        }

        /// <summary>
        /// Update the pending connection selection line position.
        /// </summary>
        public virtual void UpdatePendingConnectionLine()
        {
            if (!pendingConnectionLine) return;

            if (CurrConnectionMode == ConnectionMode.Connecting)
            {
                pendingLineStartPos = (IsStartNodeOutHandle) ? StartNode.OutConnectionPoint : StartNode.InConnectionPoint;
                pendingLineEndPos = (useMouseWorldPosition) ? Camera.main.ScreenToWorldPoint(Input.mousePosition) : Input.mousePosition;
                pendingConnectionLine.SetPositions(new Vector3[] { pendingLineStartPos, new Vector3(pendingLineEndPos.x, pendingLineEndPos.y, pendingLineStartPos.z) });
            }

            pendingConnectionLine.enabled = CurrConnectionMode == ConnectionMode.Connecting;
        }
        /// <summary>
        /// Update the pending connection deletion line position.
        /// </summary>
        public virtual void UpdatePendingDeletionLine()
        {
            if (!pendingDeletionLine) return;

            if (CurrConnectionMode == ConnectionMode.Deleting)
            {
                pendingLineStartPos = (IsStartNodeOutHandle) ? StartNode.OutConnectionPoint : StartNode.InConnectionPoint;
                pendingLineEndPos = (useMouseWorldPosition) ? Camera.main.ScreenToWorldPoint(Input.mousePosition) : Input.mousePosition;
                pendingDeletionLine.SetPositions(new Vector3[] { pendingLineStartPos, new Vector3(pendingLineEndPos.x, pendingLineEndPos.y, pendingLineStartPos.z) });
            }

            pendingDeletionLine.enabled = CurrConnectionMode == ConnectionMode.Deleting;
        }

        protected virtual void Awake()
        {
            if (!SINGLETON) SINGLETON = this;
            else
            {
                Debug.LogError($"More than one '{GetType()}' singletons found, destroying second...");
                DestroyImmediate(this);
            }
        }

        protected virtual void Update()
        {
            if (updateMethod == UnityUpdateMethods.Update)
            {
                UpdatePendingConnectionLine();
                UpdatePendingDeletionLine();
            }
        }
        protected virtual void FixedUpdate()
        {
            if (updateMethod == UnityUpdateMethods.FixedUpdate)
            {
                UpdatePendingConnectionLine();
                UpdatePendingDeletionLine();
            }
        }
    }
}
