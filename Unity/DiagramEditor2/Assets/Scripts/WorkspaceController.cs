﻿using UnityEngine;
using MattRGeorge.General.Utilities;
using MattRGeorge.Unity.Utilities;
using Assets.Scripts.Models;

namespace Assets.Scripts
{
    public class WorkspaceController : MonoBehaviour
    {
        public static WorkspaceController SINGLETON = null;

        [Tooltip("The major number of the minimum version when loading.")]
        [SerializeField] protected int minVersionMajor = 0;
        [Tooltip("The minor number of the minimum version when loading.")]
        [SerializeField] protected int minVersionMinor = 0;
        [Tooltip("The patch number of the minimum version when loading.")]
        [SerializeField] protected int minVersionPatch = 0;
        [Tooltip("The file path to save the workspace file to.")]
        [SerializeField] protected string filePath = "";
        [Tooltip("The name of the file excluding the extension and '.' for the extension.")]
        [SerializeField] protected string fileName = "workspace";
        [Tooltip("The extension to use for the saved file excluding the '.'.")]
        [SerializeField] protected string fileExt = "wrkspc";
        public StringUnityEvent OnFilePathUpdated = new StringUnityEvent();
        public StringUnityEvent OnFileNameUpdated = new StringUnityEvent();

        /// <summary>
        /// The currently saved file path for saving/loading.
        /// </summary>
        public string FilePath
        {
            get => PlayerPrefs.GetString(filePathKey, filePath);
            set
            {
                if (string.IsNullOrEmpty(value)) filePath = Application.persistentDataPath;
                else if (FileAccessUtility.DoesDirectoryOrFileExist(value)) filePath = value;
                else return;

                PlayerPrefs.SetString(filePathKey, filePath);
                OnFilePathUpdated?.Invoke(filePath);
            }
        }
        /// <summary>
        /// The currently saved file name for saving/loading.
        /// </summary>
        public string FileName
        {
            get => PlayerPrefs.GetString(fileNameKey, fileName);
            set
            {
                if (string.IsNullOrEmpty(value)) return;

                fileName = value;
                PlayerPrefs.SetString(fileNameKey, fileName);
                OnFileNameUpdated?.Invoke(fileName);
            }
        }

        protected readonly string filePathKey = "workspaceFilePath";
        protected readonly string fileNameKey = "workspaceFileName";

        /// <summary>
        /// Save the worksapce to a file.
        /// </summary>
        public virtual void Save()
        {
            if (!DiagramController.SINGLETON) return;

            DiagramModel diagramModel = DiagramController.SINGLETON.SaveRootDiagramToModel();
            WorkspaceModel model = new WorkspaceModel()
            {
                SoftwareVersion = Application.version,
                NodeIDs = DiagramController.SINGLETON.NodeIDs,
                RootDiagram = diagramModel
            };

            FileAccessUtility.SaveToBinaryFile((string.IsNullOrEmpty(FilePath)) ? Application.persistentDataPath : FilePath, $"{FileName}.{fileExt}", model);
        }
        /// <summary>
        /// Load the workspace file at the set location.
        /// </summary>
        public virtual void Load()
        {
            if (!DiagramController.SINGLETON) return;

            WorkspaceModel workspaceModel = FileAccessUtility.LoadFromBinaryFile<WorkspaceModel>((string.IsNullOrEmpty(FilePath)) ? Application.persistentDataPath : FilePath, $"{FileName}.{fileExt}");
            if (!CheckMinVersion(workspaceModel)) Debug.LogWarning("Note: Saved workspace software version less than minnimum supported version! Load may not come out properly.");
            if (workspaceModel != null) DiagramController.SINGLETON.LoadFromModel(workspaceModel.RootDiagram, workspaceModel.NodeIDs);
        }

        /// <summary>
        /// Checks the version of the given model to ensure it is at least the minnimum version.
        /// </summary>
        /// <param name="model">The model to check.</param>
        /// <returns>True if sufficent version.</returns>
        protected virtual bool CheckMinVersion(WorkspaceModel model)
        {
            SemanticVersion modelVersion = new SemanticVersion(model.SoftwareVersion);
            SemanticVersion minVersion = new SemanticVersion(minVersionMajor, minVersionMinor, minVersionPatch);
            return modelVersion >= minVersion;
        }

        protected virtual void Awake()
        {
            if (!SINGLETON) SINGLETON = this;
            else
            {
                Debug.LogError($"More than one '{GetType()}' singletons found, destroying second...");
                Destroy(this);
            }
        }
        protected virtual void Start()
        {
            Load();

            OnFilePathUpdated?.Invoke(FilePath);
            OnFileNameUpdated?.Invoke(FileName);
        }
    }
}
