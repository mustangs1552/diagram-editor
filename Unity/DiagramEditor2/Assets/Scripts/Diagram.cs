﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;
using Assets.Scripts.Models;

namespace Assets.Scripts
{
    public class Diagram : MonoBehaviour
    {
        [Tooltip("This diagram's starting name.")]
        [SerializeField] protected string diagramName = "";
        [Tooltip("The standard node object this diagram creates.")]
        [SerializeField] protected ConnectableNode standardNodeObj = null;
        [Tooltip("The start node object this diagram creates.")]
        [SerializeField] protected StartEndNode startNodeObj = null;
        [Tooltip("The end node object this diagram creates.")]
        [SerializeField] protected StartEndNode endNodeObj = null;

        /// <summary>
        /// This diagram's name.
        /// </summary>
        public string DiagramName
        {
            get
            {
                return (diagramName == null) ? "" : diagramName;
            }
            set
            {
                if (value != null) diagramName = value;
            }
        }

        protected List<ConnectableNode> nodes = new List<ConnectableNode>();

        /// <summary>
        /// Enter this diagram.
        /// </summary>
        public virtual void EnterDiagram()
        {
            if (!DiagramController.SINGLETON) return;

            DiagramController.SINGLETON.EnterDiagram(this);
        }
        /// <summary>
        /// Exit this diagram.
        /// </summary>
        public virtual void ExitDiagram()
        {
            if (!DiagramController.SINGLETON) return;

            DiagramController.SINGLETON.ExitDiagram();
        }

        /// <summary>
        /// Show this diagram's contents.
        /// </summary>
        public virtual void ShowDiagram()
        {
            ShowCurrentNodes();
        }
        /// <summary>
        ///  Hide this diagram's contents.
        /// </summary>
        public virtual void HideDiagram()
        {
            HideCurrentNodes();
        }

        /// <summary>
        /// Create a start node that doesn't have a reference node and uses the given in name.
        /// </summary>
        /// <param name="name">The name to use.</param>
        public virtual void CreateStartNode(string name)
        {
            if (string.IsNullOrEmpty(name) || !startNodeObj) return;

            GameObject newObj = ObjectPoolManager.SINGLETON.Instantiate(startNodeObj.gameObject, DiagramController.SINGLETON.DefaultNodeSpawnPos.position, Quaternion.identity, DiagramController.SINGLETON.MainCanvas.transform, true, false);
            ConnectableNode newNode = newObj.GetComponent<ConnectableNode>();
            newNode.NodeName = name;
            nodes.Add(newNode);
            newObj.SetActive(false);
        }
        /// <summary>
        /// Create a start node that references the given in node.
        /// </summary>
        /// <param name="node">The in node.</param>
        public virtual void CreateStartNode(ConnectableNode node)
        {
            if (!node || !startNodeObj) return;

            GameObject newObj = ObjectPoolManager.SINGLETON.Instantiate(startNodeObj.gameObject, DiagramController.SINGLETON.DefaultNodeSpawnPos.position, Quaternion.identity, DiagramController.SINGLETON.MainCanvas.transform, true, false);
            nodes.Add(newObj.GetComponent<ConnectableNode>());
            nodes[nodes.Count - 1].GetComponent<StartEndNode>().ReferenceNode = node;
            newObj.SetActive(false);
        }
        /// <summary>
        /// Create an end node that doesn't have a reference node and uses the given in name.
        /// </summary>
        /// <param name="name">The name to use.</param>
        public virtual void CreateEndNode(string name)
        {
            if (string.IsNullOrEmpty(name) || !endNodeObj) return;

            GameObject newObj = ObjectPoolManager.SINGLETON.Instantiate(endNodeObj.gameObject, DiagramController.SINGLETON.DefaultNodeSpawnPos.position, Quaternion.identity, DiagramController.SINGLETON.MainCanvas.transform, true, false);
            ConnectableNode newNode = newObj.GetComponent<ConnectableNode>();
            newNode.NodeName = name;
            nodes.Add(newNode);
            newObj.SetActive(false);
        }
        /// <summary>
        /// Create an end node that references the given out node.
        /// </summary>
        /// <param name="node">The out node.</param>
        public virtual void CreateEndNode(ConnectableNode node)
        {
            if (!node || !endNodeObj) return;

            GameObject newObj = ObjectPoolManager.SINGLETON.Instantiate(endNodeObj.gameObject, DiagramController.SINGLETON.DefaultNodeSpawnPos.position, Quaternion.identity, DiagramController.SINGLETON.MainCanvas.transform, true, false);
            nodes.Add(newObj.GetComponent<ConnectableNode>());
            nodes[nodes.Count - 1].GetComponent<StartEndNode>().ReferenceNode = node;
            newObj.SetActive(false);
        }

        /// <summary>
        /// Create a standard node that will belong to this diagram.
        /// </summary>
        public virtual GameObject CreateStandardNode()
        {
            if (!standardNodeObj || !DiagramController.SINGLETON) return null;

            GameObject newObj = ObjectPoolManager.SINGLETON.Instantiate(standardNodeObj.gameObject, DiagramController.SINGLETON.DefaultNodeSpawnPos.position, Quaternion.identity, DiagramController.SINGLETON.MainCanvas.transform, true, false);
            nodes.Add(newObj.GetComponent<ConnectableNode>());
            return newObj;
        }

        /// <summary>
        /// Remove the start/end node that is referencing the given in/out node.
        /// </summary>
        /// <param name="node">The in/out node.</param>
        public virtual void RemoveStartEndNode(ConnectableNode node)
        {
            if (!node) return;

            StartEndNode currStartEndNode = null;
            foreach(ConnectableNode currNode in nodes)
            {
                currStartEndNode = currNode.GetComponent<StartEndNode>();
                if (!currStartEndNode) continue;

                if(currStartEndNode.ReferenceNode == node)
                {
                    currStartEndNode.DestroyNode();
                    nodes.Remove(currNode);
                    break;
                }
            }
        }
        /// <summary>
        /// Removes the given node from the list of nodes.
        /// </summary>
        /// <param name="node">The node to remove.</param>
        public virtual void RemoveNode(ConnectableNode node)
        {
            foreach (ConnectableNode currNode in nodes)
            {
                if (currNode == node)
                {
                    nodes.Remove(node);
                    break;
                }
            }
        }

        /// <summary>
        /// Destroy the nodes in this diagram.
        /// </summary>
        public virtual void EraseDiagram()
        {
            foreach (ConnectableNode node in nodes) node.DestroyNode();
            nodes = new List<ConnectableNode>();
        }

        /// <summary>
        /// Save this diagram's information to a model.
        /// </summary>
        /// <returns>The model with the diagram's information.</returns>
        public virtual DiagramModel SaveToModel()
        {
            DiagramModel model = new DiagramModel()
            {
                Name = DiagramName
            };
            List<NodeModel> nodeModels = new List<NodeModel>();
            foreach(ConnectableNode node in nodes)
            {
                nodeModels.Add(node.SaveToModel());
            }
            model.Nodes = nodeModels;

            return model;
        }
        /// <summary>
        /// Load the information from the given diagram model to this diagram.
        /// </summary>
        /// <param name="model">The diagram model to load to this diagram.</param>
        public virtual void LoadFromModel(DiagramModel model)
        {
            if (model == null) return;

            DiagramName = model.Name;

            GameObject createdNode = null;
            foreach(NodeModel nodeModel in model.Nodes)
            {
                switch(nodeModel.Type)
                {
                    case NodeType.Node:
                    case NodeType.Connectable:
                    case NodeType.Enterable:
                        createdNode = CreateStandardNode();
                        createdNode.GetComponent<Node>().LoadFromModel(nodeModel);
                        break;
                }
            }

            ShowDiagram();
            ConnectLoadedNodesByNodeIDs();
            HideDiagram();
        }
        /// <summary>
        /// Load the information from the given diagram model to this diagram but only load the start/end nodes.
        /// </summary>
        /// <param name="diagramModel">The diagram model to load to this diagram.</param>
        public virtual void LoadStartEndNodesFromModel(DiagramModel model)
        {
            if (model == null) return;

            EnterableNode currEnterableNode = null;
            StartEndNode currStartEndNode = null;
            foreach(ConnectableNode node in nodes)
            {
                currEnterableNode = node.GetComponent<EnterableNode>();
                if(currEnterableNode)
                {
                    foreach(NodeModel nodeModel in model.Nodes)
                    {
                        if (nodeModel.Type == NodeType.Enterable && node.AreEqual(nodeModel))
                        {
                            currEnterableNode.LoadStartEndNodesByModel(nodeModel.DiagramModel);
                            break;
                        }
                    }

                    currEnterableNode = null;
                    continue;
                }

                currStartEndNode = node.GetComponent<StartEndNode>();
                if(currStartEndNode)
                {
                    foreach (NodeModel nodeModel in model.Nodes)
                    {
                        if (nodeModel.Type == NodeType.Start && currStartEndNode.IsStartNode && node.AreEqual(nodeModel))
                        {
                            node.LoadFromModel(nodeModel);
                            break;
                        }
                        else if(nodeModel.Type == NodeType.End && currStartEndNode.IsEndNode && node.AreEqual(nodeModel))
                        {
                            node.LoadFromModel(nodeModel);
                            break;
                        }
                    }

                    currStartEndNode = null;
                }
            }

            ShowDiagram();
            ConnectLoadedNodesByNodeIDs();
            HideDiagram();

            if (model.Nodes.Count != nodes.Count) DiagramController.SINGLETON.NeedsAnotherLoadPass = true;
        }

        /// <summary>
        /// Connect all the nodes to their out nodes by their IDs.
        /// </summary>
        protected virtual void ConnectLoadedNodesByNodeIDs()
        {
            ConnectableNode foundNode = null;
            foreach (ConnectableNode node in nodes)
            {
                foreach(int id in node.OutNodeIDs)
                {
                    foundNode = GetNodeByNodeID(id);
                    if (!foundNode) continue;

                    node.AddOutNode(foundNode, true);
                }
            }
        }

        /// <summary>
        /// Get the node that belongs to this diagram that goes by the given ID.
        /// </summary>
        /// <param name="nodeID">The desired node's ID.</param>
        /// <returns>The found node.</returns>
        protected virtual ConnectableNode GetNodeByNodeID(int nodeID)
        {
            if (nodeID < 0) return null;

            foreach(ConnectableNode node in nodes)
            {
                if (node.NodeID == nodeID) return node;
            }

            return null;
        }

        /// <summary>
        /// Show this diagram's nodes.
        /// </summary>
        protected virtual void ShowCurrentNodes()
        {
            foreach (ConnectableNode node in nodes)
            {
                if (!node) continue;

                node.ShowNode();
            }
        }
        /// <summary>
        /// Hide this diagram's nodes.
        /// </summary>
        protected virtual void HideCurrentNodes()
        {
            foreach (ConnectableNode node in nodes)
            {
                if (!node) continue;

                node.HideNode();
            }
        }
    }
}
