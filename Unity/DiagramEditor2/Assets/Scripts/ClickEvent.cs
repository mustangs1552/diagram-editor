﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class ClickEvent : MonoBehaviour, IPointerClickHandler
    {
        [Tooltip("The type of click that triggers this event.")]
        public PointerEventData.InputButton clickType = PointerEventData.InputButton.Left;
        [Tooltip("Log messages to the console about clicks?")]
        public bool isDebug = false;
        public UnityEvent OnClick = new UnityEvent();

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (isDebug) Debug.Log($"Clicked {gameObject.name} with {eventData.button}!");
            if (clickType == eventData.button) OnClick?.Invoke();
        }
    }
}
