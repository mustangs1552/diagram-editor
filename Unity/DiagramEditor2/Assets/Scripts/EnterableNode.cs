﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using Assets.Scripts.Models;

namespace Assets.Scripts
{
    public class EnterableNode : ConnectableNode
    {
        [Tooltip("This node's internal diagram.")]
        [SerializeField] protected Diagram diagram = null;
        [Tooltip("Create default start/end nodes in this nodes diagram if in/out connections are not allowed?")]
        [SerializeField] protected bool createDefaultStartEndNodes = false;
        [Tooltip("The name to use for the default start node from `createDefaultStartEndNodes`.")]
        [SerializeField] protected string defaultStartNodeName = "Start";
        [Tooltip("The name to use for the default end node from `createDefaultStartEndNodes`.")]
        [SerializeField] protected string defaultEndNodeName = "End";

        /// <summary>
        /// Enter this node.
        /// </summary>
        public virtual void EnterNode()
        {
            if (!diagram) return;

            diagram.EnterDiagram();
        }

        public override bool AddInNode(ConnectableNode node, bool updateOtherNode = false)
        {
            bool success = base.AddInNode(node, updateOtherNode);
            if (success && diagram) diagram.CreateStartNode(node);

            return success;
        }
        public override bool AddOutNode(ConnectableNode node, bool updateOtherNode = false)
        {
            bool success = base.AddOutNode(node, updateOtherNode);
            if (success && diagram) diagram.CreateEndNode(node);

            return success;
        }

        public override void ClearConnection(ConnectableNode otherNode, bool otherNodeIsInNode)
        {
            base.ClearConnection(otherNode, otherNodeIsInNode);

            if (!diagram) return;

            diagram.RemoveStartEndNode(otherNode);
        }

        public override void DestroyNode()
        {
            base.DestroyNode();

            if (diagram) diagram.EraseDiagram();
        }

        public override NodeModel SaveToModel()
        {
            NodeModel model = base.SaveToModel();
            model.Type = NodeType.Enterable;
            if (diagram) model.DiagramModel = diagram.SaveToModel();

            return model;
        }
        public override void LoadFromModel(NodeModel model)
        {
            base.LoadFromModel(model);
            diagram.LoadFromModel(model.DiagramModel);
        }
        /// <summary>
        /// Load the information from the given diagram model to this node's diagram but only load the start/end nodes.
        /// </summary>
        /// <param name="diagramModel">The diagram model to load to this node's diagram.</param>
        public virtual void LoadStartEndNodesByModel(DiagramModel diagramModel)
        {
            if (!diagram) return;
            diagram.LoadStartEndNodesFromModel(diagramModel);
        }

        /// <summary>
        /// Update this node's diagram's name with the given string.
        /// </summary>
        /// <param name="name">This node's diagram's new name.</param>
        protected virtual void UpdateDiagramName(string name)
        {
            diagram.DiagramName = name;
        }

        /// <summary>
        /// Create the default start/end nodes as needed.
        /// </summary>
        protected virtual void CreateDefaultStartEndNodes()
        {
            if (!diagram) return;

            if (!canBePointedTo && createDefaultStartEndNodes && !string.IsNullOrEmpty(defaultStartNodeName)) diagram.CreateStartNode(defaultStartNodeName);
            if (!canPointToOthers && createDefaultStartEndNodes && !string.IsNullOrEmpty(defaultEndNodeName)) diagram.CreateEndNode(defaultEndNodeName);
        }

        /// <summary>
        /// Check that required values are assigned.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (!diagram) UnityLoggingUtility.LogMissingValue(GetType(), "diagram", gameObject);
        }

        protected virtual void Awake()
        {
            CheckMissingValues();

            OnNameUpdated.AddListener(UpdateDiagramName);

            CreateDefaultStartEndNodes();
        }
    }
}
