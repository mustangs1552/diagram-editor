﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MattRGeorge.Unity.Utilities;
using MattRGeorge.Unity.Tools.ObjectPooling;
using Assets.Scripts.Models;

namespace Assets.Scripts
{
    public class Node : MonoBehaviour, IObjectPoolEvents
    {
        [Tooltip("The background shape of this node.")]
        [SerializeField] protected Image background = null;
        [Tooltip("The node's starting name.")]
        [SerializeField] protected string startNodeName = "";
        [Tooltip("The Text Mesh Pro input field for editing the name of this node.")]
        [SerializeField] protected TMP_InputField nameInputFieldTMP = null;
        public StringUnityEvent OnNameUpdated = new StringUnityEvent();

        /// <summary>
        /// This node's ID.
        /// </summary>
        public int NodeID => nodeID;

        /// <summary>
        /// The background shape of this node.
        /// </summary>
        public Sprite BackgroundShape
        {
            get
            {
                if (!background) return null;
                return background.sprite;
            }
            set
            {
                if (!background) return;
                background.sprite = value;
            }
        }

        /// <summary>
        /// Toggle the interactibility of this node's name Text Mesh Pro input field.
        /// </summary>
        public bool NameInputFieldInteractable
        {
            get => (!nameInputFieldTMP) ? false : nameInputFieldTMP.interactable;
            set
            {
                if (!nameInputFieldTMP) return;

                nameInputFieldTMP.interactable = value;
            }
        }
        /// <summary>
        /// Toggle the visibility of this node's name Text Mesh Pro input field's background.
        /// </summary>
        public bool ShowTextFieldBackground
        {
            get => (!nameInputFieldTMP || !nameInputFieldTMP.targetGraphic) ? false : showTextFieldBackground;
            set
            {
                if (!nameInputFieldTMP || !nameInputFieldTMP.targetGraphic) return;

                showTextFieldBackground = value;
                if (!showTextFieldBackground)
                {
                    if (!startInputFieldBackgroundColor)
                    {
                        inputFieldBackgroundColor = nameInputFieldTMP.targetGraphic.color;
                        startInputFieldBackgroundColor = true;
                    }
                    nameInputFieldTMP.targetGraphic.color = new Color(0, 0, 0, 0);
                }
                else nameInputFieldTMP.targetGraphic.color = inputFieldBackgroundColor;
            }
        }

        /// <summary>
        /// The name of this node.
        /// </summary>
        public string NodeName
        {
            get => nodeName;
            set
            {
                if (value == null) nodeName = "";
                else
                {
                    nodeName = value;
                    if (nameInputFieldTMP.text != nodeName) nameInputFieldTMP.text = nodeName;
                    OnNameUpdated?.Invoke(nodeName);
                }
            }
        }

        protected int nodeID = -1;
        protected bool showTextFieldBackground = false;
        protected Color inputFieldBackgroundColor = Color.white;
        protected bool startInputFieldBackgroundColor = false;
        protected string nodeName = "";

        /// <summary>
        /// Is this node and the given node equal?
        /// </summary>
        /// <param name="otherNode">The node to compare.</param>
        /// <returns>True if considered equal.</returns>
        public virtual bool AreEqual(Node otherNode)
        {
            return otherNode && (NodeName == otherNode.NodeName || NodeID == otherNode.NodeID);
        }
        /// <summary>
        /// Is this node and the given node model equal?
        /// </summary>
        /// <param name="nodeModel">The node model to compare.</param>
        /// <returns>True if considered equal.</returns>
        public virtual bool AreEqual(NodeModel nodeModel)
        {
            return nodeModel != null && (NodeName == nodeModel.Name || NodeID == nodeModel.NodeID);
        }

        public virtual void OnInstantiatedByObjectPool()
        {
            gameObject.SetActive(true);
            nodeID = DiagramController.SINGLETON.GetAvailableNodeID(true);
            NodeName = startNodeName;
        }

        public virtual void OnDestroyedByObjectPool()
        {
            
        }

        /// <summary>
        /// Save this node's information to a model.
        /// </summary>
        /// <returns>The model with the node's information.</returns>
        public virtual NodeModel SaveToModel()
        {
            NodeModel model = new NodeModel()
            {
                Type = NodeType.Node,
                NodeID = nodeID,
                Name = NodeName,
                PositionX = transform.position.x,
                PositionY = transform.position.y,
                PositionZ = transform.position.z
            };
            return model;
        }
        /// <summary>
        /// Load the information from the given node model to this node.
        /// </summary>
        /// <param name="model">The node model to load to this node.</param>
        public virtual void LoadFromModel(NodeModel model)
        {
            if (model == null) return;

            nodeID = model.NodeID;
            NodeName = model.Name;
            transform.position = new Vector3(model.PositionX, model.PositionY, model.PositionZ);
        }

        /// <summary>
        /// Set listeners for events that need to run.
        /// </summary>
        protected virtual void SetListeners()
        {
            if (UIController.SINGLETON)
            {
                nameInputFieldTMP.onSelect.AddListener(x => UIController.SINGLETON.IsInTextBox = true);
                nameInputFieldTMP.onDeselect.AddListener(x => UIController.SINGLETON.IsInTextBox = false);
            }
        }

        protected virtual void Start()
        {
            ShowTextFieldBackground = false;
            SetListeners();
        }
    }
}