﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Utilities.Static;
using Assets.Scripts.Models;

namespace Assets.Scripts
{
    public class DiagramController : MonoBehaviour
    {
        public static DiagramController SINGLETON = null;

        [Tooltip("The root diagram.")]
        [SerializeField] protected Diagram rootDiagram = null;
        [Tooltip("The default spawn position on all nodes.")]
        [SerializeField] protected Transform defaultNodeSpawnPos = null;
        [Tooltip("The main canvas that nodes are spawned on.")]
        [SerializeField] protected Canvas mainCanvas = null;
        [Tooltip("Max number of extra attempts to load a diagram from a file.")]
        [SerializeField] protected int maxExtraLoadPassesAllowed = 5;
        public UnityEvent OnDiagramChanged = new UnityEvent();

        /// <summary>
        /// The main canvas that nodes are spawned on.
        /// </summary>
        public Canvas MainCanvas => mainCanvas;

        /// <summary>
        /// The currently active diagram.
        /// </summary>
        public Diagram CurrDiagram => (diagramPath.Count > 0) ? diagramPath[diagramPath.Count - 1] : null;
        /// <summary>
        /// Is the current diagram the root diagram?
        /// </summary>
        public bool IsRootDiagram => diagramPath.Count == 1;
        /// <summary>
        /// Get the names of the currently entered diagram path.
        /// </summary>
        public List<string> DiagramNames
        {
            get
            {
                List<string> names = new List<string>();
                foreach (Diagram diagram in diagramPath) names.Add(diagram.DiagramName);
                return names;
            }
        }

        /// <summary>
        /// The default spawn position on all nodes.
        /// Returns this object's position if null.
        /// </summary>
        public Transform DefaultNodeSpawnPos => (!defaultNodeSpawnPos) ? transform : defaultNodeSpawnPos;

        /// <summary>
        /// The list of used node IDs.
        /// </summary>
        public HashSet<int> NodeIDs
        {
            get
            {
                return(nodeIDs == null) ? new HashSet<int>() : new HashSet<int>(nodeIDs);
            }
            protected set
            {
                if (value != null) nodeIDs = value;
            }
        }

        /// <summary>
        /// Is another pass to load a diagram from a file needed?
        /// </summary>
        public bool NeedsAnotherLoadPass { get; set; } = false;

        protected List<Diagram> diagramPath = new List<Diagram>();
        protected HashSet<int> nodeIDs = new HashSet<int>();
        protected int currExtraLoadPass = 0;

        /// <summary>
        /// Enter the given diagram.
        /// </summary>
        /// <param name="diagram">The diagram to enter.</param>
        public virtual void EnterDiagram(Diagram diagram)
        {
            if (!diagram) return;

            HideCurrDiagram();
            diagramPath.Add(diagram);
            ShowCurrDiagram();

            OnDiagramChanged?.Invoke();
        }
        /// <summary>
        /// Enter the diagram at the given index of the diagram path.
        /// </summary>
        /// <param name="index">The index of the desired diagram in the diagram path.</param>
        public virtual void EnterDiagramInPath(int index)
        {
            if (index < 0 || index >= diagramPath.Count) return;

            ExitDiagram(diagramPath.Count - 1 - index);
        }

        /// <summary>
        /// Exit the currently active diagram.
        /// </summary>
        public virtual void ExitDiagram(int exitCount = 1)
        {
            if (diagramPath.Count <= 1 || exitCount < 1 || exitCount >= diagramPath.Count) return;

            HideCurrDiagram();
            for (int i = 0; i < exitCount; i++) diagramPath.RemoveAt(diagramPath.Count - 1);
            ShowCurrDiagram();

            OnDiagramChanged?.Invoke();
        }

        /// <summary>
        /// Gets the next unused ID to be used for a node.
        /// </summary>
        /// <param name="addToIDList">Add the returned ID to the list right away?</param>
        /// <returns>The next unused ID.</returns>
        public virtual int GetAvailableNodeID(bool addToIDList)
        {
            bool found = false;
            int i = 0;
            HashSet<int> existingNodeIDs = NodeIDs;
            while(!found)
            {
                if (!existingNodeIDs.Contains(i))
                {
                    if (addToIDList)
                    {
                        existingNodeIDs.Add(i);
                        NodeIDs = existingNodeIDs;
                    }
                    found = true;
                }
                else i++;
            }

            return i;
        }

        /// <summary>
        /// Create the currently active diagram's standard node.
        /// </summary>
        public virtual void CreateStandardNodeInCurrDiagram()
        {
            CurrDiagram.CreateStandardNode();
        }

        /// <summary>
        /// Tells the current diagram to remove the given node from its list.
        /// </summary>
        /// <param name="node">The node to remove.</param>
        public virtual void DestroyingNode(ConnectableNode node)
        {
            if (!CurrDiagram) return;

            CurrDiagram.RemoveNode(node);
        }

        /// <summary>
        /// Show the currently active diagram.
        /// </summary>
        public virtual void ShowCurrDiagram()
        {
            if (!CurrDiagram) return;

            CurrDiagram.ShowDiagram();
        }
        /// <summary>
        /// Hide the currently active diagram.
        /// </summary>
        public virtual void HideCurrDiagram()
        {
            if (!CurrDiagram) return;

            CurrDiagram.HideDiagram();
        }

        /// <summary>
        /// Save the root diagram's information to a model.
        /// </summary>
        /// <returns>The model with the root diagram's information.</returns>
        public virtual DiagramModel SaveRootDiagramToModel()
        {
            if (!rootDiagram) return new DiagramModel();

            return rootDiagram.SaveToModel();
        }
        /// <summary>
        /// Load the information from the given diagram model to the root diagram.
        /// </summary>
        /// <param name="model">The diagram model to load to the root diagram.</param>
        /// <param name="nodeIDs">The list of used node IDs to load.</param>
        public virtual void LoadFromModel(DiagramModel rootDiagramModel, HashSet<int> nodeIDs)
        {
            if (rootDiagramModel == null || !rootDiagram) return;

            rootDiagram.LoadFromModel(rootDiagramModel);
            rootDiagram.LoadStartEndNodesFromModel(rootDiagramModel);
            while(NeedsAnotherLoadPass && currExtraLoadPass < maxExtraLoadPassesAllowed)
            {
                NeedsAnotherLoadPass = false;
                currExtraLoadPass++;
                rootDiagram.LoadStartEndNodesFromModel(rootDiagramModel);
            }
            NodeIDs = nodeIDs;

            ShowCurrDiagram();
            currExtraLoadPass = 0;
        }

        /// <summary>
        /// Check that required values are assigned.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (!rootDiagram) UnityLoggingUtility.LogMissingValue(GetType(), "rootDiagram", gameObject);
            if (!mainCanvas) UnityLoggingUtility.LogMissingValue(GetType(), "mainCanvas", gameObject);
        }

        protected virtual void Awake()
        {
            if (!SINGLETON) SINGLETON = this;
            else
            {
                Debug.LogError($"More than one '{GetType()}' singletons found, destroying second...");
                Destroy(this);
            }

            CheckMissingValues();

            EnterDiagram(rootDiagram);
        }
    }
}
