﻿using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class DraggableItem2D : MonoBehaviour
    {
        [Tooltip("The target transform to be dragged (uses this transform is null).")]
        [SerializeField] protected Transform target = null;
        [Tooltip("Update drag on Update or FixedUpdate?")]
        public UnityUpdateMethods pollingMethod = UnityUpdateMethods.Update;
        [Tooltip("Use mouse's position in the world or screen space?")]
        public bool useMouseWorldPosition = false;
        [Tooltip("Drag via left click?")]
        public bool onLeftClick = true;
        [Tooltip("Drag via right click?")]
        public bool onRightClick = false;
        [Tooltip("Drag via scroll wheel click?")]
        public bool onScrollWheelSclick = false;

        /// <summary>
        /// The target transform to be dragged (uses this transform is null).
        /// </summary>
        public Transform Target => (!target) ? transform : target;

        protected bool isDragging = false;
        protected Vector2 lastMousePos = Vector2.zero;

        /// <summary>
        /// Start dragging the set object.
        /// </summary>
        public virtual void StartDragging()
        {
            if ((!onLeftClick && Input.GetMouseButton(0)) || (!onRightClick && Input.GetMouseButton(1)) || (!onScrollWheelSclick && Input.GetMouseButton(2))) return;

            isDragging = true;
            if (isDragging) lastMousePos = (useMouseWorldPosition) ? Camera.main.ScreenToWorldPoint(Input.mousePosition) : Input.mousePosition;
        }
        /// <summary>
        /// Stop dragging the set object.
        /// </summary>
        public virtual void StopDragging()
        {
            isDragging = false;
        }

        /// <summary>
        /// Update the position of the target being dragged.
        /// </summary>
        public virtual void Drag()
        {
            if (!isDragging) return;

            Vector2 currMousePos = (useMouseWorldPosition) ? Camera.main.ScreenToWorldPoint(Input.mousePosition) : Input.mousePosition;
            Target.Translate(currMousePos - lastMousePos);
            lastMousePos = (useMouseWorldPosition) ? Camera.main.ScreenToWorldPoint(Input.mousePosition) : Input.mousePosition;
        }

        protected virtual void Update()
        {
            if (pollingMethod == UnityUpdateMethods.Update) Drag();
        }
        protected virtual void FixedUpdate()
        {
            if (pollingMethod == UnityUpdateMethods.FixedUpdate) Drag();
        }
    }
}
