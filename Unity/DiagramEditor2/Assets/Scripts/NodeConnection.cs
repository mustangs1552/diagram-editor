﻿using UnityEngine;
using MattRGeorge.Unity.Utilities;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace Assets.Scripts
{
    public class NodeConnection : MonoBehaviour
    {
        [Tooltip("The line renderer that this connection uses to show its connection.")]
        public LineRenderer lineRenderer = null;
        [Tooltip("What Unity update method should update this connection's position?")]
        public UnityUpdateMethods updateMethod = UnityUpdateMethods.Update;

        /// <summary>
        /// The starting node that this line starts at.
        /// </summary>
        public ConnectableNode StartNode => startNode;
        /// <summary>
        /// The ending node that this line ends at.
        /// </summary>
        public ConnectableNode EndNode => endNode;

        protected ConnectableNode startNode = null;
        protected ConnectableNode endNode = null;

        /// <summary>
        /// Set the two nodes that this connection is to be connected between.
        /// </summary>
        /// <param name="start">The starting node.</param>
        /// <param name="end">The ending node.</param>
        public virtual void SetNodes(ConnectableNode start, ConnectableNode end)
        {
            if (!start || !end || start == end) return;

            startNode = start;
            endNode = end;

            UpdateLine();
        }
        
        /// <summary>
        /// Clear the nodes so this line no longer connects to anything.
        /// </summary>
        public virtual void ClearNodes()
        {
            startNode = null;
            endNode = null;
            UpdateLine();
        }

        /// <summary>
        /// Update the line's position and destroy it if it doesn't have both a start and end node.
        /// </summary>
        public virtual void UpdateLine()
        {
            if (!lineRenderer) return;
            if (!startNode || !endNode || !startNode.isActiveAndEnabled || !endNode.isActiveAndEnabled)
            {
                if (ObjectPoolManager.SINGLETON) ObjectPoolManager.SINGLETON.Destroy(gameObject);
                return;
            }

            lineRenderer.SetPositions(new Vector3[] { startNode.OutConnectionPoint, endNode.InConnectionPoint });
        }

        protected virtual void Update()
        {
            if (updateMethod == UnityUpdateMethods.Update) UpdateLine();
        }
        protected virtual void FixedUpdate()
        {
            if (updateMethod == UnityUpdateMethods.FixedUpdate) UpdateLine();
        }
    }
}
