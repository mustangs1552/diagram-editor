﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    [Serializable]
    public class WorkspaceModel
    {
        public string SoftwareVersion { get; set; } = "";
        public HashSet<int> NodeIDs { get; set; } = new HashSet<int>();
        public DiagramModel RootDiagram { get; set; } = new DiagramModel();
    }
}
