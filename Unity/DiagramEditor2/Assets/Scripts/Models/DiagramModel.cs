﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    [Serializable]
    public class DiagramModel
    {
        public string Name { get; set; } = "";
        public List<NodeModel> Nodes { get; set; } = new List<NodeModel>();
    }
}
