﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    public enum NodeType
    {
        Node,
        Connectable,
        Enterable,
        Start,
        End
    }

    [Serializable]
    public class NodeModel
    {
        public NodeType Type { get; set; } = NodeType.Node;
        public int NodeID { get; set; } = -1;
        public string Name { get; set; } = "";
        public float PositionX { get; set; } = 0;
        public float PositionY { get; set; } = 0;
        public float PositionZ { get; set; } = 0;

        public List<int> OutNodeIDs { get; set; } = new List<int>();

        public DiagramModel DiagramModel { get; set; } = new DiagramModel();
    }
}
