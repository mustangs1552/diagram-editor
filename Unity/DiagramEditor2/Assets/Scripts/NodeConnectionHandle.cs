﻿using UnityEngine;

namespace Assets.Scripts
{
    public class NodeConnectionHandle : MonoBehaviour
    {
        [Tooltip("Is this handle an out connection handle?")]
        public bool isOutHandle = true;
        [Tooltip("The node that this handle belongs to.")]
        [SerializeField] protected ConnectableNode thisNode = null;

        /// <summary>
        /// Start/end a connection command of a connection between two nodes.
        /// </summary>
        public void ConnectionSelection()
        {
            if (NodeConnectionController.SINGLETON.CurrConnectionMode == ConnectionMode.None)
            {
                if (!thisNode) return;

                NodeConnectionController.SINGLETON.StartNode = thisNode;
                NodeConnectionController.SINGLETON.IsStartNodeOutHandle = isOutHandle;
                NodeConnectionController.SINGLETON.CurrConnectionMode = ConnectionMode.Connecting;
            }
            else if (NodeConnectionController.SINGLETON.CurrConnectionMode == ConnectionMode.Connecting)
            {
                if (!thisNode || !NodeConnectionController.SINGLETON.StartNode || NodeConnectionController.SINGLETON.IsStartNodeOutHandle == isOutHandle)
                {
                    NodeConnectionController.SINGLETON.CurrConnectionMode = ConnectionMode.None;
                    NodeConnectionController.SINGLETON.StartNode = null;
                    return;
                }

                if (NodeConnectionController.SINGLETON.IsStartNodeOutHandle) NodeConnectionController.SINGLETON.StartNode.AddOutNode(thisNode, true);
                else NodeConnectionController.SINGLETON.StartNode.AddInNode(thisNode, true);

                NodeConnectionController.SINGLETON.CurrConnectionMode = ConnectionMode.None;
                NodeConnectionController.SINGLETON.StartNode = null;
            }
        }

        /// <summary>
        /// Start/end a deletion command of a connection between two nodes.
        /// </summary>
        public void ConnectionDeletion()
        {
            if (NodeConnectionController.SINGLETON.CurrConnectionMode == ConnectionMode.None)
            {
                if (!thisNode || (thisNode.InNodes.Count == 0 && !isOutHandle) || (thisNode.OutNodes.Count == 0 && isOutHandle)) return;

                NodeConnectionController.SINGLETON.StartNode = thisNode;
                NodeConnectionController.SINGLETON.IsStartNodeOutHandle = isOutHandle;
                NodeConnectionController.SINGLETON.CurrConnectionMode = ConnectionMode.Deleting;
            }
            else if (NodeConnectionController.SINGLETON.CurrConnectionMode == ConnectionMode.Deleting)
            {
                if (!thisNode || !NodeConnectionController.SINGLETON.StartNode || NodeConnectionController.SINGLETON.IsStartNodeOutHandle == isOutHandle)
                {
                    NodeConnectionController.SINGLETON.CurrConnectionMode = ConnectionMode.None;
                    NodeConnectionController.SINGLETON.StartNode = null;
                    return;
                }

                NodeConnectionController.SINGLETON.StartNode.ClearConnection(thisNode, isOutHandle);

                NodeConnectionController.SINGLETON.CurrConnectionMode = ConnectionMode.None;
                NodeConnectionController.SINGLETON.StartNode = null;
            }
        }
    }
}
