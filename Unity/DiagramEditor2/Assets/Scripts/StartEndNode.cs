﻿using Assets.Scripts.Models;

namespace Assets.Scripts
{
    public class StartEndNode : ConnectableNode
    {
        /// <summary>
        /// The node that this node represents from the next level up.
        /// </summary>
        public ConnectableNode ReferenceNode
        {
            get => referenceNode;
            set
            {
                referenceNode = value;
                if (referenceNode) NodeName = referenceNode.NodeName;
            }
        }
        public bool IsStartNode => CanPointToOthers && !CanBePointedTo;
        public bool IsEndNode => !CanPointToOthers && CanBePointedTo;

        protected ConnectableNode referenceNode = null;

        public override void DestroyNode()
        {
            base.DestroyNode();

            ReferenceNode = null;
        }

        public void OnEnable()
        {
            if (ReferenceNode) NodeName = ReferenceNode.NodeName;
        }

        public override NodeModel SaveToModel()
        {
            NodeModel model = base.SaveToModel();
            model.Type = (IsStartNode) ? NodeType.Start : NodeType.End;

            return model;
        }
    }
}
