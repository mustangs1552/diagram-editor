﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace MattRGeorge.General.Utilities
{
    public static class FileAccessUtility
    {
        /// <summary>
        /// Does the given directory/file exist?
        /// </summary>
        /// <param name="path">The path of directory.file to check.</param>
        /// <returns></returns>
        public static bool DoesDirectoryOrFileExist(string path)
        {
            if (string.IsNullOrEmpty(path)) return false;

            return Directory.Exists(path) || File.Exists(path);
        }

        /// <summary>
        /// Saves the given `System.Serializable` class to the given file path with the given filename as abinary file.
        /// </summary>
        /// <typeparam name="T">The `System.Serializable` class.</typeparam>
        /// <param name="path">The file path to save to.</param>
        /// <param name="fileName">The filename.</param>
        /// <param name="data">The class object to save.</param>
        /// <returns>True if successful.</returns>
        public static bool SaveToBinaryFile<T>(string path, string fileName, T data)
        {
            if (string.IsNullOrEmpty(path) || string.IsNullOrEmpty(fileName) || data == null) return false;

            string fullPath = Path.Combine(path, fileName);
            using (FileStream stream = new FileStream(fullPath, FileMode.OpenOrCreate))
            {
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, data);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return true;
        }
        /// <summary>
        /// Loads the given `System.Serializable` class located at the given file path with the given filename from a binary file.
        /// </summary>
        /// <typeparam name="T">The `System.Serializable` class.</typeparam>
        /// <param name="path">The file path the file is located at.</param>
        /// <param name="fileName">The filename.</param>
        /// <returns>The loaded class object.</returns>
        public static T LoadFromBinaryFile<T>(string path, string fileName)
        {
            if (string.IsNullOrEmpty(path) || string.IsNullOrEmpty(fileName)) return default;

            string fullPath = Path.Combine(path, fileName);
            if (!File.Exists(fullPath)) return default;

            T data = default;
            using(FileStream stream = new FileStream(fullPath, FileMode.Open))
            {
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    data = (T)formatter.Deserialize(stream);
                }
                catch(Exception e)
                {
                    throw e;
                }
            }

            return data;
        }
    }
}
