﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.Unity.Utilities.Static;
using Assets.Scripts.Models;

namespace Assets.Scripts
{
    public class ConnectableNode : Node
    {
        [Tooltip("Can this node point to other nodes?")]
        [SerializeField] protected bool canPointToOthers = true;
        [Tooltip("Can this node be pointed to by other nodes?")]
        [SerializeField] protected bool canBePointedTo = true;
        [Tooltip("The node connection object to use when connecting to another node.")]
        [SerializeField] protected NodeConnection nodeConnection = null;
        [Tooltip("The connection handle for when other nodes want to point to this node.")]
        [SerializeField] protected NodeConnectionHandle inConnectionHandle = null;
        [Tooltip("The connection handle for when this node wants to point to other nodes.")]
        [SerializeField] protected NodeConnectionHandle outConnectionHandle = null;

        /// <summary>
        /// Can this node point to other nodes?
        /// </summary>
        public bool CanPointToOthers
        {
            get => canPointToOthers;
            set
            {
                canPointToOthers = value;
                if (outConnectionHandle) outConnectionHandle.gameObject.SetActive(canPointToOthers);
            }
        }
        /// <summary>
        /// Can this node be pointed to by other nodes?
        /// </summary>
        public bool CanBePointedTo
        {
            get => canBePointedTo;
            set
            {
                canBePointedTo = value;
                if (inConnectionHandle) inConnectionHandle.gameObject.SetActive(canBePointedTo);
            }
        }

        /// <summary>
        /// All the nodes pointing to this node.
        /// Returns duplicate.
        /// </summary>
        public List<ConnectableNode> InNodes
        {
            get => new List<ConnectableNode>(inNodes);
            set
            {
                if (value == null) inNodes = new List<ConnectableNode>();
                inNodes = ListUtility.RemoveNullUnityObjectEntries(value);
            }
        }
        /// <summary>
        /// All the nodes that this node points to.
        /// Returns duplicate.
        /// </summary>
        public List<ConnectableNode> OutNodes
        {
            get => new List<ConnectableNode>(outNodes);
            set
            {
                if (value == null) outNodes = new List<ConnectableNode>();
                outNodes = ListUtility.RemoveNullUnityObjectEntries(value);
            }
        }

        /// <summary>
        /// The position of the in connection handle.
        /// </summary>
        public Vector3 InConnectionPoint => (!inConnectionHandle) ? transform.position : inConnectionHandle.transform.position;
        /// <summary>
        /// The position of the out connection handle.
        /// </summary>
        public Vector3 OutConnectionPoint => (!outConnectionHandle) ? transform.position : outConnectionHandle.transform.position;

        /// <summary>
        /// The IDs of the out nodes.
        /// </summary>
        public List<int> OutNodeIDs => (outNodeIDs != null) ? outNodeIDs : new List<int>();

        protected List<ConnectableNode> inNodes = new List<ConnectableNode>();
        protected List<ConnectableNode> outNodes = new List<ConnectableNode>();
        protected List<NodeConnection> outConnections = new List<NodeConnection>();
        protected bool otherNodeSuccess = false;
        protected List<int> outNodeIDs = new List<int>();

        /// <summary>
        /// Show this node and any out connection lines.
        /// </summary>
        public virtual void ShowNode()
        {
            gameObject.SetActive(true);
            foreach (NodeConnection conn in outConnections) conn.gameObject.SetActive(true);
        }
        /// <summary>
        /// Hide this node and any out connection lines.
        /// </summary>
        public virtual void HideNode()
        {
            gameObject.SetActive(false);
            foreach (NodeConnection conn in outConnections) conn.gameObject.SetActive(false);
        }

        public override bool AreEqual(Node otherNode)
        {
            if (!base.AreEqual(otherNode)) return false;

            ConnectableNode otherConnNode = otherNode.GetComponent<ConnectableNode>();
            bool areEqual = otherConnNode && inNodes.Count == otherConnNode.InNodes.Count && outNodes.Count == otherConnNode.OutNodes.Count;
            if (!areEqual) return false;

            foreach(ConnectableNode node in inNodes)
            {
                foreach(ConnectableNode otherInNode in otherConnNode.InNodes)
                {
                    areEqual = node.AreEqual(otherInNode);
                    if (!areEqual) return false;
                }
            }
            foreach (ConnectableNode node in outNodes)
            {
                foreach (ConnectableNode otherOutNode in otherConnNode.OutNodes)
                {
                    areEqual = node.AreEqual(otherOutNode);
                    if (!areEqual) return false;
                }
            }

            return otherConnNode && base.AreEqual(otherNode) && areEqual;
        }

        /// <summary>
        /// Add a node that wants to point to this node.
        /// </summary>
        /// <param name="node">The node that wants to point to this node.</param>
        /// <param name="updateOtherNode">Automatically add this node as a connection to the other node?</param>
        /// <returns>True if successful.</returns>
        public virtual bool AddInNode(ConnectableNode node, bool updateOtherNode = false)
        {
            if (!CanBePointedTo || !node || node == this || inNodes.Contains(node)) return false;

            if (updateOtherNode)
            {
                otherNodeSuccess = node.AddOutNode(this);
                if (!otherNodeSuccess) return false;
            }

            inNodes.Add(node);
            return true;
        }
        /// <summary>
        /// Add a node that this node wants to point to.
        /// </summary>
        /// <param name="node">The node that this node wants to point to.</param>
        /// <param name="updateOtherNode">Automatically add this node as a connection to the other node?</param>
        /// <returns>True if successful.</returns>
        public virtual bool AddOutNode(ConnectableNode node, bool updateOtherNode = false)
        {
            if (!CanPointToOthers || !node || node == this || outNodes.Contains(node)) return false;

            if (updateOtherNode)
            {
                otherNodeSuccess = node.AddInNode(this);
                if (!otherNodeSuccess) return false;
            }

            outNodes.Add(node);
            if (!outNodeIDs.Contains(node.NodeID)) outNodeIDs.Add(node.NodeID);
            UpdateOutConnections();
            return true;
        }

        /// <summary>
        /// Clear the connection between this node and the given node.
        /// </summary>
        /// <param name="otherNode">The node to clear the connection to.</param>
        /// <param name="otherNodeIsInNode">Is the passed "otherNode" an in node or an out node?</param>
        public virtual void ClearConnection(ConnectableNode otherNode, bool otherNodeIsInNode)
        {
            if (!otherNode || (inNodes.Count == 0 && outNodes.Count == 0)) return;

            if (otherNodeIsInNode && inNodes.Contains(otherNode))
            {
                inNodes.Remove(otherNode);
                otherNode.ClearConnection(this, false);
            }
            else if (!otherNodeIsInNode && outNodes.Contains(otherNode))
            {
                outNodes.Remove(otherNode);
                outNodeIDs.Remove(otherNode.NodeID);
                otherNode.ClearConnection(this, true);
                foreach (NodeConnection conn in outConnections)
                {
                    if ((conn.StartNode == this && conn.EndNode == otherNode) || (conn.EndNode == this && conn.StartNode == otherNode))
                    {
                        conn.ClearNodes();
                        outConnections.Remove(conn);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Destroys this node and its connections to other nodes.
        /// </summary>
        public virtual void DestroyNode()
        {
            if (!ObjectPoolManager.SINGLETON) return;

            List<ConnectableNode> temp = new List<ConnectableNode>(inNodes);
            foreach (ConnectableNode otherNode in temp) ClearConnection(otherNode, true);
            temp = new List<ConnectableNode>(outNodes);
            foreach (ConnectableNode otherNode in temp) ClearConnection(otherNode, false);
            ObjectPoolManager.SINGLETON.Destroy(gameObject);

            if (DiagramController.SINGLETON) DiagramController.SINGLETON.DestroyingNode(this);
        }

        public override NodeModel SaveToModel()
        {
            NodeModel model = base.SaveToModel();
            model.Type = NodeType.Connectable;
            model.OutNodeIDs = OutNodeIDs;

            return model;
        }
        public override void LoadFromModel(NodeModel model)
        {
            base.LoadFromModel(model);

            outNodeIDs = model.OutNodeIDs;
        }

        /// <summary>
        /// Creates connection lines for nodes that this node connects to that don't yet have one and removes connection lines that point to other nodes that this node no longer points to.
        /// </summary>
        protected virtual void UpdateOutConnections()
        {
            List<ConnectableNode> doneNodes = new List<ConnectableNode>();
            foreach (NodeConnection conn in outConnections)
            {
                if (!outNodes.Contains(conn.EndNode))
                {
                    if (ObjectPoolManager.SINGLETON) ObjectPoolManager.SINGLETON.Destroy(conn.gameObject);
                }
                else doneNodes.Add(conn.EndNode);
            }

            if (!nodeConnection) return;
            foreach (ConnectableNode node in outNodes)
            {
                if (!doneNodes.Contains(node) && ObjectPoolManager.SINGLETON)
                {
                    NodeConnection newConn = ObjectPoolManager.SINGLETON.Instantiate(nodeConnection.gameObject).GetComponent<NodeConnection>();
                    newConn.SetNodes(this, node);
                    outConnections.Add(newConn);
                    newConn.gameObject.SetActive(true);
                }
            }
        }

        protected override void Start()
        {
            base.Start();

            CanPointToOthers = canPointToOthers;
            CanBePointedTo = canBePointedTo;
        }
    }
}
