# Changelog

Format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
Project uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

To see pre-NPM hosted changelog look at: preNPMHosted_CHANGELOG.md.

## [0.0.0] - 8/26/20
### [Added]
- Changelog and started tracking the version.
- Basic and connectable nodes.