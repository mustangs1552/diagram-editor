﻿function Rectangle(x, y, w, h, fill)
{
    this.startX = x || 0;
    this.startY = y || 0;
    this.startW = w || 1;
    this.startH = h || 1;
    this.x = this.startX;
    this.y = this.startY;
    this.w = this.startW;
    this.h = this.startH;
    this.fill = fill || "#AAAAAA";
}

Rectangle.prototype.Draw = function (context, zoomLevel)
{
    context.fillStyle = this.fill;
    this.w = this.startW * zoomLevel;
    this.h = this.startH * zoomLevel;
    this.x = this.x - (this.w - this.startW);
    this.y = this.y - (this.h - this.startH);
    context.fillRect(this.x, this.y, this.w, this.h);
}
Rectangle.prototype.Contains = function (mouseX, mouseY)
{
    return (this.x <= mouseX) && (this.x + this.w >= mouseX) && (this.y <= mouseY) && (this.y + this.h >= mouseY);
}