﻿function CanvasState(canvas)
{
    //Setup
    this.canvas = canvas;
    this.context = canvas.getContext("2d");

    let stylePaddingLeft = 0;
    let stylePaddingTop = 0;
    let styleBorderLeft = 0;
    let styleBorderTop = 0;
    if (document.defaultView && document.defaultView.getComputedStyle)
    {
        this.stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)["paddingLeft"], 10) || 0;
        this.stylePaddingTop = parseInt(document.defaultView.getComputedStyle(canvas, null)["paddingTop"], 10) || 0;
        this.styleBorderLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)["borderLeftWidth"], 10) || 0;
        this.styleBorderTop = parseInt(document.defaultView.getComputedStyle(canvas, null)["borderTopWidth"], 10) || 0;
    }

    let html = document.body.parentNode;
    this.htmlTop = html.offsetTop;
    this.htmlLeft = html.offsetLeft;

    this.needsRedraw = true;
    this.objects = [];
    this.dragging = false;
    this.selection = null;
    this.dragOffX = 0;
    this.dragOffY = 0;
    this.lastMouseX = 0;
    this.lastMouseY = 0;
    this.zoomLevel = 1;
    this.buttonToBePressed = null;

    let state = this;

    // Events
    canvas.addEventListener("selectStart", function (e)
    {
        e.preventDefault();
        return false;
    }, false);
    canvas.addEventListener("mousedown", function (e)
    {
        let mouse = state.GetMouse(e);
        state.lastMouseX = mouse.x;
        state.lastMouseY = mouse.y;
        let objs = state.objects;
        for (let i = objs.length - 1; i >= 0; i--)
        {
            if (objs[i].Pressed != undefined && objs[i].Contains(mouse.x, mouse.y))
            {
                state.buttonToBePressed = objs[i];
                return;
            }
            else if (objs[i].Contains(mouse.x, mouse.y))
            {
                let mySel = objs[i];
                state.dragOffX = mouse.x - mySel.x;
                state.dragOffY = mouse.y - mySel.y;
                state.dragging = true;
                state.selection = mySel;
                state.needsRedraw = true;
                return;
            }
        }
        state.dragging = true;

        if (state.selection)
        {
            state.selection = null;
            state.needsRedraw = true;
        }
    }, true);
    canvas.addEventListener("mousemove", function (e)
    {
        if (state.dragging)
        {
            let mouse = state.GetMouse(e);
            if (state.selection != null)
            {
                state.selection.x = mouse.x - state.dragOffX;
                state.selection.y = mouse.y - state.dragOffY;
            }
            else
            {
                let objs = state.objects;
                for (let i = 0; i < objs.length; i++)
                {
                    objs[i].x += mouse.x - state.lastMouseX;
                    objs[i].y += mouse.y - state.lastMouseY;
                }
            }
            state.lastMouseX = mouse.x;
            state.lastMouseY = mouse.y;

            state.needsRedraw = true;
        }
    }, true);
    canvas.addEventListener("mouseup", function (e)
    {
        let mouse = state.GetMouse(e);
        if (state.buttonToBePressed != null && state.buttonToBePressed.Contains(mouse.x, mouse.y))
        {
            state.buttonToBePressed.Pressed();
        }

        state.buttonToBePressed = null;
        state.dragging = false;
        state.lastMouseX = 0;
        state.lastMouseY = 0;
    }, true);

    canvas.addEventListener("wheel", function (e)
    {
        if (e.deltaY < 0) state.SetZoom(state.zoomLevel + .5);
        else if (e.deltaY > 0) state.SetZoom(state.zoomLevel - .5);

        return false;
    }, true);

    canvas.addEventListener("dblclick", function (e)
    {
        console.log("Double clicked!");
    }, true);

    // Other settings
    this.selectionColor = "#CC0000";
    this.selectionWidth = 2;

    // Draw loop
    this.interval = 1;
    setInterval(function ()
    {
        state.Draw();
    }, state.interval);
}

CanvasState.prototype.Clear = function ()
{
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
}
CanvasState.prototype.Draw = function ()
{
    if (this.needsRedraw)
    {
        let context = this.context;
        let objs = this.objects;
        this.Clear();

        for (let i = 0; i < objs.length; i++)
        {
            let obj = objs[i];
            if (obj.x > this.canvas.width || obj.y > this.canvas.height || obj.x + obj.w < 0 || obj.y + obj.h < 0) continue;
            obj.Draw(context, this.zoomLevel);
        }

        if (this.selection != null)
        {
            context.strokeStyle = this.selectionColor;
            context.lineWidth = this.selectionWidth;
            let mySel = this.selection;
            context.strokeRect(mySel.x, mySel.y, mySel.w, mySel.h);
        }

        this.currZoomlevel = this.zoomLevel
        this.needsRedraw = false;
    }
}

CanvasState.prototype.SetZoom = function (zoomLevel)
{
    if (zoomLevel >= .1) this.zoomLevel = zoomLevel;
    else this.zoomLevel = .1;

    this.needsRedraw = true;
}

CanvasState.prototype.GetMouse = function (e)
{
    let element = this.canvas;
    let offsetX = 0;
    let offsetY = 0
    let mouseX = 0;
    let mouseY = 0;

    if (element.offsetParent !== undefined)
    {
        do
        {
            offsetX += element.offsetLeft;
            offsetY += element.offsetTop;
        }
        while (element = element.offsetParent);
    }

    offsetX += this.stylePaddingLeft + this.styleBorderLeft + this.htmlLeft;
    offsetY += this.stylePaddingTop + this.styleBorderTop + this.htmlTop;

    mouseX = e.pageX - offsetX;
    mouseY = e.pageY - offsetY;

    return { x: mouseX, y: mouseY };
}

CanvasState.prototype.SpawnObject = function (object)
{
    this.objects.push(object);
    this.needsRedraw = true;
}