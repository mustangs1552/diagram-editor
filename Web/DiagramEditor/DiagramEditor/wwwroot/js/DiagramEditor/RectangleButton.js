﻿function RectangleButton(x, y, w, h, fill)
{
    this.startX = x || 0;
    this.startY = y || 0;
    this.startW = w || 1;
    this.startH = h || 1;
    this.x = this.startX;
    this.y = this.startY;
    this.w = this.startW;
    this.h = this.startH;
    this.fill = fill || "#AAAAAA";
}

RectangleButton.prototype = Object.create(Rectangle.prototype);
RectangleButton.prototype.Pressed = function ()
{
    console.log("Pressed!");
}