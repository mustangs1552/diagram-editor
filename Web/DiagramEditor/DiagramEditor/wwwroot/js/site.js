﻿let s = new CanvasState(document.getElementById('diagramEditorCanvas'));
s.SpawnObject(new RectangleButton(40, 40, 50, 50)); // The default is gray
s.SpawnObject(new Rectangle(60, 140, 40, 60, 'lightskyblue'));
// Lets make some partially transparent
s.SpawnObject(new Rectangle(80, 150, 60, 30, 'rgba(127, 255, 212, .5)'));
s.SpawnObject(new Rectangle(125, 80, 30, 80, 'rgba(245, 222, 179, .7)'));